import {
  MarkdownElementTag,
  MarkdownTree,
  Metadata,
  parseMarkdown,
} from '../src/index';

type WordBuilder = (depth: number) => MarkdownTree;

const makeWordBuilder =
  (tag: MarkdownElementTag) =>
  (text: string) =>
  (depth: number): MarkdownTree => {
    return {
      tag,
      text,
      depth,
    };
  };

const appendWordBuilderChildren =
  (builder: WordBuilder, children: (WordBuilder | WordBuilder[])[]) =>
  (depth: number): MarkdownTree => {
    const node = builder(depth);
    node.children = [
      ...(node.children || []),
      ...children.map((child) => {
        if (Array.isArray(child)) {
          return line(depth + 1, child);
        } else {
          return child(depth + 1);
        }
      }),
    ];
    return node;
  };

const textWordBuilder = makeWordBuilder('text');
const boldWordBuilder = makeWordBuilder('bold');
const italicWordBuilder = makeWordBuilder('italic');
const boldItalicBuilder = makeWordBuilder('bold-italic');
const strikeThroughWordBuilder = makeWordBuilder('strikethrough');
const codePhraseBuilder = makeWordBuilder('code');
const makeLinkOrImageBuilder =
  (tag: 'image' | 'link') =>
  (text: string, metadata: Metadata) =>
  (depth: number): MarkdownTree => {
    const node = makeWordBuilder(tag)(text)(depth);
    node.metadata = metadata;
    return node;
  };
const linkBuilder = makeLinkOrImageBuilder('link');
const imageBuilder = makeLinkOrImageBuilder('image');

const makeElement =
  (tag: MarkdownElementTag) =>
  (depth: number, words: WordBuilder[]): MarkdownTree => {
    return {
      tag,
      depth,
      children: words.map((word) => word(depth + 1)),
    };
  };
const line = makeElement('line');
const blockQuote = (
  depth: number,
  lines: (WordBuilder | WordBuilder[])[]
): MarkdownTree => {
  return {
    tag: 'blockquote',
    depth,
    children: lines.map((currentLine) => {
      if (Array.isArray(currentLine)) {
        return line(depth + 1, currentLine);
      } else {
        return currentLine(depth + 1);
      }
    }),
  };
};

const wrappedElement =
  (elementMaker: (depth: number, children: WordBuilder[]) => MarkdownTree) =>
  (tag: MarkdownElementTag) =>
  (depth: number, lines: (WordBuilder | WordBuilder[])[]): MarkdownTree => {
    return {
      tag,
      depth,
      children: lines.map((words) => {
        if (Array.isArray(words)) {
          return elementMaker(depth + 1, words);
        } else {
          return words(depth + 1);
        }
      }),
    };
  };

const wrappedLine = wrappedElement(line);

const paragraph = wrappedLine('paragraph');
const codeLine = wrappedLine('code');
const header1 = wrappedLine('h1');
const header2 = wrappedLine('h2');
const header4 = wrappedLine('h4');
const header5 = wrappedLine('h5');
const listItem = wrappedLine('li');

const list = (
  tag: 'ol' | 'ul',
  depth: number,
  words: (WordBuilder | WordBuilder[])[]
): MarkdownTree => {
  return {
    tag,
    depth,
    children: words.map((item) => {
      if (Array.isArray(item)) {
        return listItem(depth + 1, item);
      } else {
        return item(depth + 1);
      }
    }),
  };
};

const ROOT_DEPTH = 0;

describe('parse lines of text', () => {
  test('parse markdown text line', () => {
    expect(parseMarkdown('Line 1')).toEqual([
      paragraph(ROOT_DEPTH, [[textWordBuilder('Line 1')]]),
    ]);
    expect(parseMarkdown("Italicized text is the *cat's meow*.")).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('Italicized text is the '),
          italicWordBuilder("cat's meow"),
          textWordBuilder('.'),
        ],
      ]),
    ]);
    expect(parseMarkdown("Italicized text is the _cat's meow_.")).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('Italicized text is the '),
          italicWordBuilder("cat's meow"),
          textWordBuilder('.'),
        ],
      ]),
    ]);
    expect(parseMarkdown('A*cat*meow')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('A'),
          italicWordBuilder('cat'),
          textWordBuilder('meow'),
        ],
      ]),
    ]);
    expect(parseMarkdown('*cat*meow')).toEqual([
      paragraph(ROOT_DEPTH, [
        [italicWordBuilder('cat'), textWordBuilder('meow')],
      ]),
    ]);
    expect(parseMarkdown('A_cat_meow')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('A'),
          textWordBuilder('_'),
          textWordBuilder('cat'),
          textWordBuilder('_'),
          textWordBuilder('meow'),
        ],
      ]),
    ]);
    expect(parseMarkdown('I just love **bold text**.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('I just love '),
          boldWordBuilder('bold text'),
          textWordBuilder('.'),
        ],
      ]),
    ]);
    expect(parseMarkdown('I just love __bold text__.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('I just love '),
          boldWordBuilder('bold text'),
          textWordBuilder('.'),
        ],
      ]),
    ]);
    expect(parseMarkdown('Love**is**bold')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('Love'),
          boldWordBuilder('is'),
          textWordBuilder('bold'),
        ],
      ]),
    ]);
    expect(parseMarkdown('Love__is__bold')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('Love'),
          textWordBuilder('_'),
          textWordBuilder('_'),
          textWordBuilder('is'),
          textWordBuilder('_'),
          textWordBuilder('_'),
          textWordBuilder('bold'),
        ],
      ]),
    ]);
    expect(parseMarkdown('This text is ***really important***.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('This text is '),
          boldItalicBuilder('really important'),
          textWordBuilder('.'),
        ],
      ]),
    ]);
    expect(parseMarkdown('This text is ___really important___.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('This text is '),
          boldItalicBuilder('really important'),
          textWordBuilder('.'),
        ],
      ]),
    ]);
    expect(parseMarkdown('This is really***very***important text.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('This is really'),
          boldItalicBuilder('very'),
          textWordBuilder('important text.'),
        ],
      ]),
    ]);
    expect(parseMarkdown('This is really___very___important text.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('This is really'),
          textWordBuilder('_'),
          textWordBuilder('_'),
          textWordBuilder('_'),
          textWordBuilder('very'),
          textWordBuilder('_'),
          textWordBuilder('_'),
          textWordBuilder('_'),
          textWordBuilder('important text.'),
        ],
      ]),
    ]);
  });
  test('parse markdown text multiline and paragraphs', () => {
    expect(parseMarkdown('Line 1\nLine 2')).toEqual([
      paragraph(ROOT_DEPTH, [
        [textWordBuilder('Line 1')],
        [textWordBuilder('Line 2')],
      ]),
    ]);
    expect(
      parseMarkdown('Line 1\nLine 2\n\nLine 3\nLine 4\n\nLine 5\nLine 6')
    ).toEqual([
      paragraph(ROOT_DEPTH, [
        [textWordBuilder('Line 1')],
        [textWordBuilder('Line 2')],
      ]),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, [
        [textWordBuilder('Line 3')],
        [textWordBuilder('Line 4')],
      ]),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, [
        [textWordBuilder('Line 5')],
        [textWordBuilder('Line 6')],
      ]),
    ]);
  });
});

describe('parse markdown headers', () => {
  it('parses a header on a single line', () => {
    expect(parseMarkdown('# Line 1')).toEqual([
      header1(ROOT_DEPTH, [[textWordBuilder('Line 1')]]),
    ]);
    expect(parseMarkdown('##### Line 1')).toEqual([
      header5(ROOT_DEPTH, [[textWordBuilder('Line 1')]]),
    ]);
  });
  test('parse invalid markdown headers', () => {
    expect(parseMarkdown('######### Line 1')).toEqual([
      paragraph(ROOT_DEPTH, [[textWordBuilder('######### Line 1')]]),
    ]);
    expect(parseMarkdown('###Line 1')).toEqual([
      paragraph(ROOT_DEPTH, [[textWordBuilder('###Line 1')]]),
    ]);
  });
  it('parses the alternate header syntax for h1 and h2', () => {
    expect(parseMarkdown('Line 1\n=====\n\nLine 2')).toEqual([
      header1(ROOT_DEPTH, [[textWordBuilder('Line 1')]]),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, [[textWordBuilder('Line 2')]]),
    ]);
  });
  it('parses headers containing other elements', () => {
    expect(
      parseMarkdown('# [asd](https://www.github.com) ~~strike~~ **bold**')
    ).toEqual([
      header1(ROOT_DEPTH, [
        [
          linkBuilder('asd', {
            href: 'https://www.github.com',
          }),
          textWordBuilder(' '),
          strikeThroughWordBuilder('strike'),
          textWordBuilder(' '),
          boldWordBuilder('bold'),
        ],
      ]),
    ]);
  });
});

describe('parse blockquotes', function () {
  it('parses a single line', () => {
    const singleLine =
      '> Dorothy followed her through many of the beautiful rooms in her castle.';
    expect(parseMarkdown(singleLine)).toEqual([
      blockQuote(ROOT_DEPTH, [
        [
          textWordBuilder(
            'Dorothy followed her through many of the beautiful rooms in her castle.'
          ),
        ],
      ]),
    ]);
  });
  it('parses a single line with space between delimiters', () => {
    const singleLine =
      '>> Dorothy followed her through many of the beautiful rooms in her castle.';
    expect(parseMarkdown(singleLine)).toEqual([
      blockQuote(ROOT_DEPTH, [
        (depth) =>
          blockQuote(depth, [
            [
              textWordBuilder(
                'Dorothy followed her through many of the beautiful rooms in her castle.'
              ),
            ],
          ]),
      ]),
    ]);
  });
  it('parses multiple lines', () => {
    const multiLine = `\
> Dorothy followed her through many of the beautiful rooms in her castle.
>
> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.`;
    expect(parseMarkdown(multiLine)).toEqual([
      blockQuote(ROOT_DEPTH, [
        [
          textWordBuilder(
            'Dorothy followed her through many of the beautiful rooms in her castle.'
          ),
        ],
        [],
        [
          textWordBuilder(
            'The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.'
          ),
        ],
      ]),
    ]);
  });
  it('parses multiple lines with nesting', () => {
    const multiLineNested = `\
> Dorothy followed her through many of the beautiful rooms in her castle.
>
>> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.
`;
    const multiLineNestedBQ = blockQuote(ROOT_DEPTH, [
      [
        textWordBuilder(
          'Dorothy followed her through many of the beautiful rooms in her castle.'
        ),
      ],
      [],
      (depth: number) =>
        blockQuote(depth, [
          [
            textWordBuilder(
              'The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.'
            ),
          ],
        ]),
    ]);
    expect(parseMarkdown(multiLineNested)).toEqual([
      multiLineNestedBQ,
      paragraph(ROOT_DEPTH, []),
    ]);
  });
  it('parses multiple lines with nested elements', () => {
    const multiElement = `\
> #### The quarterly results look great!
>
>
>  *Everything* is going according to **plan**.`;
    const multiElementBQ = blockQuote(ROOT_DEPTH, [
      (depth) =>
        header4(depth, [
          [
            textWordBuilder('The quarterly results look great'),
            textWordBuilder('!'),
          ],
        ]),
      [],
      [],
      [
        textWordBuilder(' '),
        italicWordBuilder('Everything'),
        textWordBuilder(' is going according to '),
        boldWordBuilder('plan'),
        textWordBuilder('.'),
      ],
    ]);
    expect(parseMarkdown(multiElement)).toEqual([multiElementBQ]);
  });
});

const lineFrom = (words: WordBuilder) => (depth: number) =>
  line(depth, [words]);

describe('parse ordered list', function () {
  it('parses an ordered list', function () {
    const expected: MarkdownTree = list('ol', ROOT_DEPTH, [
      [lineFrom(textWordBuilder('First item'))],
      [lineFrom(textWordBuilder('Second item'))],
      [lineFrom(textWordBuilder('Third item'))],
      [lineFrom(textWordBuilder('Fourth item'))],
    ]);
    expect(
      parseMarkdown(`\
1. First item
2. Second item
3. Third item
4. Fourth item`)
    ).toEqual([expected]);
    expect(
      parseMarkdown(`\
1. First item
1. Second item
1. Third item
1. Fourth item`)
    ).toEqual([expected]);
    expect(
      parseMarkdown(`\
1. First item
8. Second item
3. Third item
5. Fourth item`)
    ).toEqual([expected]);
  });
  it('parses an ordered nested list', function () {
    const expected: MarkdownTree = list('ol', ROOT_DEPTH, [
      [lineFrom(textWordBuilder('First item'))],
      [lineFrom(textWordBuilder('Second item'))],
      [
        lineFrom(textWordBuilder('Third item')),
        (depth) =>
          list('ol', depth, [
            [lineFrom(textWordBuilder('Indented item'))],
            [lineFrom(textWordBuilder('Indented item'))],
          ]),
      ],
      [lineFrom(textWordBuilder('Fourth item'))],
    ]);
    expect(
      parseMarkdown(`\
1. First item
2. Second item
3. Third item
    1. Indented item
    2. Indented item
4. Fourth item`)
    ).toEqual([expected]);
  });
});

describe('parse unordered list', function () {
  it('parses an unordered list', function () {
    const expected: MarkdownTree = list('ul', ROOT_DEPTH, [
      [lineFrom(textWordBuilder('First item'))],
      [lineFrom(textWordBuilder('Second item'))],
      [lineFrom(textWordBuilder('Third item'))],
      [lineFrom(textWordBuilder('Fourth item'))],
    ]);
    expect(
      parseMarkdown(`\
- First item
- Second item
- Third item
- Fourth item`)
    ).toEqual([expected]);
    expect(
      parseMarkdown(`\
* First item
* Second item
* Third item
* Fourth item`)
    ).toEqual([expected]);
    expect(
      parseMarkdown(`\
+ First item
+ Second item
+ Third item
+ Fourth item`)
    ).toEqual([expected]);
  });
  it('parses an unordered nested list', function () {
    const expected: MarkdownTree = list('ul', ROOT_DEPTH, [
      [lineFrom(textWordBuilder('First item'))],
      [lineFrom(textWordBuilder('Second item'))],
      [
        lineFrom(textWordBuilder('Third item')),
        (depth) =>
          list('ul', depth, [
            [lineFrom(textWordBuilder('Indented item'))],
            [lineFrom(textWordBuilder('Indented item'))],
          ]),
      ],
      [lineFrom(textWordBuilder('Fourth item'))],
    ]);
    expect(
      parseMarkdown(`\
- First item
- Second item
- Third item
    - Indented item
    - Indented item
- Fourth item`)
    ).toEqual([expected]);
  });
  it('parses an unordered list with nested ol', function () {
    const expected: MarkdownTree = list('ul', ROOT_DEPTH, [
      [lineFrom(textWordBuilder('First item'))],
      [lineFrom(textWordBuilder('Second item'))],
      [
        lineFrom(textWordBuilder('Third item')),
        (depth) =>
          list('ol', depth, [
            [lineFrom(textWordBuilder('Indented item'))],
            [lineFrom(textWordBuilder('Indented item'))],
          ]),
      ],
      [lineFrom(textWordBuilder('Fourth item'))],
    ]);
    expect(
      parseMarkdown(`\
- First item
- Second item
- Third item
    1. Indented item
    2. Indented item
- Fourth item`)
    ).toEqual([expected]);
  });
  it('parses nested lists ul > ol > ul', function () {
    const expected: MarkdownTree = list('ul', ROOT_DEPTH, [
      [lineFrom(textWordBuilder('First item'))],
      [lineFrom(textWordBuilder('Second item'))],
      [
        lineFrom(textWordBuilder('Third item')),
        (depth) =>
          list('ol', depth, [
            [lineFrom(textWordBuilder('Indented item'))],
            [
              lineFrom(textWordBuilder('Indented item')),
              (depth) =>
                list('ul', depth, [
                  [lineFrom(textWordBuilder('Indented item'))],
                  [lineFrom(textWordBuilder('Indented item'))],
                ]),
            ],
          ]),
      ],
      [lineFrom(textWordBuilder('Fourth item'))],
    ]);
    expect(
      parseMarkdown(`\
- First item
- Second item
- Third item
    1. Indented item
    2. Indented item
        + Indented item
        + Indented item
- Fourth item`)
    ).toEqual([expected]);
  });
});

describe('parsing a code block', function () {
  it('parses a single line with a code phrase', () => {
    expect(parseMarkdown('At the command prompt, type `nano`.')).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('At the command prompt, type '),
          codePhraseBuilder('nano'),
          textWordBuilder('.'),
        ],
      ]),
    ]);
  });
  it('parses a single line code phrase with escaped back ticks', () => {
    expect(parseMarkdown('``Use `code` in your Markdown file.``')).toEqual([
      paragraph(ROOT_DEPTH, [
        [codePhraseBuilder('Use `code` in your Markdown file.')],
      ]),
    ]);
  });
  it('parses a block of code', () => {
    expect(
      parseMarkdown(`\
    <html>
      <head>
      </head>
    </html>`)
    ).toEqual([
      codeLine(ROOT_DEPTH, [
        [textWordBuilder('<html>')],
        [textWordBuilder('  <head>')],
        [textWordBuilder('  </head>')],
        [textWordBuilder('</html>')],
      ]),
    ]);
  });
  it('parses an italicized line after a code block', () => {
    expect(
      parseMarkdown(`\
## Code Block

      <div>
        <p>A paragraph</p>
      </div>


      A line
      Another line
      A third line



*Sample text taken from demo of [SimpleMDE Markdown Editor](https://simplemde.com/)*
`)
    ).toEqual([
      header2(ROOT_DEPTH, [[textWordBuilder('Code Block')]]),
      paragraph(ROOT_DEPTH, []),
      codeLine(ROOT_DEPTH, [
        [textWordBuilder('  <div>')],
        [textWordBuilder('    <p>A paragraph</p>')],
        [textWordBuilder('  </div>')],
      ]),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, []),
      codeLine(ROOT_DEPTH, [
        [textWordBuilder('  A line')],
        [textWordBuilder('  Another line')],
        [textWordBuilder('  A third line')],
      ]),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, []),
      paragraph(ROOT_DEPTH, [
        [
          appendWordBuilderChildren(
            italicWordBuilder(
              'Sample text taken from demo of [SimpleMDE Markdown Editor](https://simplemde.com/)'
            ),
            [
              textWordBuilder('Sample text taken from demo of '),
              linkBuilder('SimpleMDE Markdown Editor', {
                href: 'https://simplemde.com/',
              }),
            ]
          ),
        ],
      ]),
      paragraph(ROOT_DEPTH, []),
    ]);
  });
});

const horizontalRule = (depth: number): MarkdownTree => {
  return {
    tag: 'hr',
    depth,
  };
};

describe('parsing a horizontal rule', () => {
  const expected = horizontalRule(ROOT_DEPTH);
  it('parses a line from stars', () => {
    expect(parseMarkdown('***')).toEqual([expected]);
  });
  it('parses a line from hyphens', () => {
    expect(parseMarkdown('---')).toEqual([expected]);
  });
  it('parses a line from underscores', () => {
    expect(parseMarkdown('___________________')).toEqual([expected]);
  });
});

describe('parsing links', () => {
  it('parses a link without a title', () => {
    expect(
      parseMarkdown(
        'My favorite search engine is [Duck Duck Go](https://duckduckgo.com).'
      )
    ).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('My favorite search engine is '),
          linkBuilder('Duck Duck Go', {
            href: 'https://duckduckgo.com',
          }),
          textWordBuilder('.'),
        ],
      ]),
    ]);
  });
  it('parses a link with a title', () => {
    expect(
      parseMarkdown(
        'My favorite search engine is [Duck Duck Go](https://duckduckgo.com "The best search engine for privacy").'
      )
    ).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          textWordBuilder('My favorite search engine is '),
          linkBuilder('Duck Duck Go', {
            href: 'https://duckduckgo.com',
            title: 'The best search engine for privacy',
          }),
          textWordBuilder('.'),
        ],
      ]),
    ]);
  });
  it('parses link or email shorthand', () => {
    expect(
      parseMarkdown(`\
<https://www.markdownguide.org>
<fake@example.com>`)
    ).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          linkBuilder('https://www.markdownguide.org', {
            href: 'https://www.markdownguide.org',
          }),
        ],
        [
          linkBuilder('fake@example.com', {
            href: 'mailto:fake@example.com',
          }),
        ],
      ]),
    ]);
  });
  it('parses an image url', () => {
    expect(
      parseMarkdown(
        '![The San Juan Mountains are beautiful!](/assets/images/san-juan-mountains.jpg "San Juan Mountains")'
      )
    ).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          imageBuilder('The San Juan Mountains are beautiful!', {
            href: '/assets/images/san-juan-mountains.jpg',
            title: 'San Juan Mountains',
          }),
        ],
      ]),
    ]);
  });
  it('parses an image nested in a link', () => {
    expect(
      parseMarkdown(
        '[![An old rock in the desert](/assets/images/shiprock.jpg "Shiprock, New Mexico by Beau Rogers")](https://www.flickr.com/photos/beaurogers/31833779864/in/photolist-Qv3rFw-34mt9F-a9Cmfy-5Ha3Zi-9msKdv-o3hgjr-hWpUte-4WMsJ1-KUQ8N-deshUb-vssBD-6CQci6-8AFCiD-zsJWT-nNfsgB-dPDwZJ-bn9JGn-5HtSXY-6CUhAL-a4UTXB-ugPum-KUPSo-fBLNm-6CUmpy-4WMsc9-8a7D3T-83KJev-6CQ2bK-nNusHJ-a78rQH-nw3NvT-7aq2qf-8wwBso-3nNceh-ugSKP-4mh4kh-bbeeqH-a7biME-q3PtTf-brFpgb-cg38zw-bXMZc-nJPELD-f58Lmo-bXMYG-bz8AAi-bxNtNT-bXMYi-bXMY6-bXMYv)'
      )
    ).toEqual([
      paragraph(ROOT_DEPTH, [
        [
          appendWordBuilderChildren(
            linkBuilder(
              '![An old rock in the desert](/assets/images/shiprock.jpg "Shiprock, New Mexico by Beau Rogers")',
              {
                href: 'https://www.flickr.com/photos/beaurogers/31833779864/in/photolist-Qv3rFw-34mt9F-a9Cmfy-5Ha3Zi-9msKdv-o3hgjr-hWpUte-4WMsJ1-KUQ8N-deshUb-vssBD-6CQci6-8AFCiD-zsJWT-nNfsgB-dPDwZJ-bn9JGn-5HtSXY-6CUhAL-a4UTXB-ugPum-KUPSo-fBLNm-6CUmpy-4WMsc9-8a7D3T-83KJev-6CQ2bK-nNusHJ-a78rQH-nw3NvT-7aq2qf-8wwBso-3nNceh-ugSKP-4mh4kh-bbeeqH-a7biME-q3PtTf-brFpgb-cg38zw-bXMZc-nJPELD-f58Lmo-bXMYG-bz8AAi-bxNtNT-bXMYi-bXMY6-bXMYv',
              }
            ),
            [
              imageBuilder('An old rock in the desert', {
                title: 'Shiprock, New Mexico by Beau Rogers',
                href: '/assets/images/shiprock.jpg',
              }),
            ]
          ),
        ],
      ]),
    ]);
  });
});
