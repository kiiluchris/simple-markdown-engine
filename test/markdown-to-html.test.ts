import { parseMarkdown } from '../src/index';
import { HtmlConverter } from '../src/converters/html';

const htmlConverter = new HtmlConverter();

describe('convert parse lines of text to html', () => {
  test('convert to html markdown text line', () => {
    expect(htmlConverter.convert(parseMarkdown('Line 1'))).toEqual(
      '<p>Line 1</p>'
    );
    expect(
      htmlConverter.convert(
        parseMarkdown("Italicized text is the *cat's meow*.")
      )
    ).toEqual(`<p>Italicized text is the <em>cat's meow</em>.</p>`);
    expect(
      htmlConverter.convert(
        parseMarkdown("Italicized text is the _cat's meow_.")
      )
    ).toEqual(`<p>Italicized text is the <em>cat's meow</em>.</p>`);
    expect(htmlConverter.convert(parseMarkdown('A*cat*meow'))).toEqual(
      `<p>A<em>cat</em>meow</p>`
    );
    expect(htmlConverter.convert(parseMarkdown('*cat*meow'))).toEqual(
      `<p><em>cat</em>meow</p>`
    );
    expect(htmlConverter.convert(parseMarkdown('A_cat_meow'))).toEqual(
      `<p>A_cat_meow</p>`
    );
    expect(
      htmlConverter.convert(parseMarkdown('I just love **bold text**.'))
    ).toEqual(`<p>I just love <strong>bold text</strong>.</p>`);
    expect(
      htmlConverter.convert(parseMarkdown('I just love __bold text__.'))
    ).toEqual(`<p>I just love <strong>bold text</strong>.</p>`);
    expect(htmlConverter.convert(parseMarkdown('Love**is**bold'))).toEqual(
      `<p>Love<strong>is</strong>bold</p>`
    );
    expect(htmlConverter.convert(parseMarkdown('Love__is__bold'))).toEqual(
      `<p>Love__is__bold</p>`
    );
    expect(
      htmlConverter.convert(
        parseMarkdown('This text is ***really important***.')
      )
    ).toEqual(
      `<p>This text is <strong><em>really important</em></strong>.</p>`
    );
    expect(
      htmlConverter.convert(
        parseMarkdown('This text is ___really important___.')
      )
    ).toEqual(
      `<p>This text is <strong><em>really important</em></strong>.</p>`
    );
    expect(
      htmlConverter.convert(
        parseMarkdown('This is really***very***important text.')
      )
    ).toEqual(
      `<p>This is really<strong><em>very</em></strong>important text.</p>`
    );
    expect(
      htmlConverter.convert(
        parseMarkdown('This is really___very___important text.')
      )
    ).toEqual(`<p>This is really___very___important text.</p>`);
  });
  test('convert markdown text multiline and paragraphs', () => {
    expect(htmlConverter.convert(parseMarkdown('Line 1\nLine 2'))).toEqual(
      `<p>Line 1<br/>Line 2</p>`
    );
    expect(
      htmlConverter.convert(
        parseMarkdown('Line 1\nLine 2\n\nLine 3\nLine 4\n\nLine 5\nLine 6')
      )
    ).toEqual(
      `<p>Line 1<br/>Line 2</p>
<p>Line 3<br/>Line 4</p>
<p>Line 5<br/>Line 6</p>`
    );
  });
});

describe('convert to html markdown headers', () => {
  it('converts to html a header on a single line', () => {
    expect(htmlConverter.convert(parseMarkdown('# Line 1'))).toEqual(
      `<h1>Line 1</h1>`
    );
    expect(htmlConverter.convert(parseMarkdown('##### Line 1'))).toEqual(
      `<h5>Line 1</h5>`
    );
  });
  test('convert to html invalid markdown headers', () => {
    expect(htmlConverter.convert(parseMarkdown('######### Line 1'))).toEqual(
      `<p>######### Line 1</p>`
    );
    expect(htmlConverter.convert(parseMarkdown('###Line 1'))).toEqual(
      `<p>###Line 1</p>`
    );
  });
  it('converts to html the alternate header syntax for h1 and h2', () => {
    expect(
      htmlConverter.convert(parseMarkdown('Line 1\n=====\n\nLine 2'))
    ).toEqual(
      `\
<h1>Line 1</h1>
<p>Line 2</p>`
    );
  });
});

describe('convert to html blockquotes', function () {
  it('converts to html a single line', () => {
    const singleLine =
      '> Dorothy followed her through many of the beautiful rooms in her castle.';
    expect(htmlConverter.convert(parseMarkdown(singleLine))).toBe(
      `\
<blockquote>
<p>Dorothy followed her through many of the beautiful rooms in her castle.</p>
</blockquote>`
    );
  });
  it('converts to html multiple lines', () => {
    const multiLine = `\
> Dorothy followed her through many of the beautiful rooms in her castle.
>
> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.`;
    expect(htmlConverter.convert(parseMarkdown(multiLine))).toEqual(
      `\
<blockquote>
<p>Dorothy followed her through many of the beautiful rooms in her castle.</p>
<p>The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.</p>
</blockquote>\
`
    );
  });
  it('converts to html multiple lines with nesting', () => {
    const multiLineNested = `\
> Dorothy followed her through many of the beautiful rooms in her castle.
>
>> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.
`;
    expect(htmlConverter.convert(parseMarkdown(multiLineNested))).toEqual(
      `\
<blockquote>
<p>Dorothy followed her through many of the beautiful rooms in her castle.</p>
<blockquote>
<p>The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.</p>
</blockquote>
</blockquote>\
`
    );
  });
  it('converts to html multiple lines with nested elements', () => {
    const multiElement = `\
> #### The quarterly results look great!
>
>
>  *Everything* is going according to **plan**.`;
    expect(htmlConverter.convert(parseMarkdown(multiElement))).toEqual(
      `\
<blockquote>
<h4>The quarterly results look great!</h4>
<p> <em>Everything</em> is going according to <strong>plan</strong>.</p>
</blockquote>\
`
    );
  });
});

describe('convert to html ordered list', function () {
  it('converts to html an ordered list', function () {
    const expected = `\
<ol>
<li>First item</li>
<li>Second item</li>
<li>Third item</li>
<li>Fourth item</li>
</ol>\
`;
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
1. First item
2. Second item
3. Third item
4. Fourth item`)
      )
    ).toEqual(expected);
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
1. First item
1. Second item
1. Third item
1. Fourth item`)
      )
    ).toEqual(expected);
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
1. First item
8. Second item
3. Third item
5. Fourth item`)
      )
    ).toEqual(expected);
  });
  it('converts to html an ordered nested list', function () {
    const expected = `\
<ol>
<li>First item</li>
<li>Second item</li>
<li>Third item
<ol>
<li>Indented item</li>
<li>Indented item</li>
</ol></li>
<li>Fourth item</li>
</ol>\
`;
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
  1. First item
  2. Second item
  3. Third item
      1. Indented item
      2. Indented item
  4. Fourth item`)
      )
    ).toEqual(expected);
  });
});

describe('convert to html unordered list', function () {
  it('converts to html an unordered list', function () {
    const expected = `\
<ul>
<li>First item</li>
<li>Second item</li>
<li>Third item</li>
<li>Fourth item</li>
</ul>\
`;
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
- First item
- Second item
- Third item
- Fourth item`)
      )
    ).toEqual(expected);
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
* First item
* Second item
* Third item
* Fourth item`)
      )
    ).toEqual(expected);
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
+ First item
+ Second item
+ Third item
+ Fourth item`)
      )
    ).toEqual(expected);
  });
  it('converts to html an unordered nested list', function () {
    const expected = `\
<ul>
<li>First item</li>
<li>Second item</li>
<li>Third item
<ul>
<li>Indented item</li>
<li>Indented item</li>
</ul></li>
<li>Fourth item</li>
</ul>\
`;
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
- First item
- Second item
- Third item
    - Indented item
    - Indented item
- Fourth item`)
      )
    ).toEqual(expected);
  });
});

describe('convert to html a code block', function () {
  it('converts to html a single line with a code phrase', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown('At the command prompt, type `nano`.')
      )
    ).toEqual(`<p>At the command prompt, type <code>nano</code>.</p>`);
  });
  it('converts to html a single line code phrase with escaped back ticks', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown('``Use `code` in your Markdown file.``')
      )
    ).toEqual(`<p><code>Use \`code\` in your Markdown file.</code></p>`);
  });
  it('converts to html a block of code', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
    <html>
      <head>
      </head>
    </html>`)
      )
    ).toEqual(`\
<pre><code>
&lt;html&gt;\
<br/>\
  &lt;head&gt;\
<br/>\
  &lt;/head&gt;\
<br/>\
&lt;/html&gt;
</code></pre>\
`);
  });
});

describe('parsing a horizontal rule', () => {
  const expected = '<hr/>';
  it('converts to html a line from stars', () => {
    expect(htmlConverter.convert(parseMarkdown('***'))).toEqual(expected);
  });
  it('converts to html a line from hyphens', () => {
    expect(htmlConverter.convert(parseMarkdown('---'))).toEqual(expected);
  });
  it('converts to html a line from underscores', () => {
    expect(htmlConverter.convert(parseMarkdown('___________________'))).toEqual(
      expected
    );
  });
});

describe('parsing links', () => {
  it('converts to html a link without a title', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown(
          'My favorite search engine is [Duck Duck Go](https://duckduckgo.com).'
        )
      )
    ).toEqual(`\
<p>My favorite search engine is <a href="https://duckduckgo.com">Duck Duck Go</a>.</p>\
`);
  });
  it('converts to html a link with a title', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown(
          'My favorite search engine is [Duck Duck Go](https://duckduckgo.com "The best search engine for privacy").'
        )
      )
    ).toEqual(`\
<p>My favorite search engine is <a href="https://duckduckgo.com" title="The best search engine for privacy">Duck Duck Go</a>.</p>\
`);
  });
  it('parses link or email shorthand', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown(`\
<https://www.markdownguide.org>
<fake@example.com>`)
      )
    ).toEqual(`\
<p><a href="https://www.markdownguide.org">https://www.markdownguide.org</a>\
<br/>\
<a href="mailto:fake@example.com">fake@example.com</a></p>\
`);
  });
  it('converts to html an image url', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown(
          '![The San Juan Mountains are beautiful!](/assets/images/san-juan-mountains.jpg "San Juan Mountains")'
        )
      )
    ).toEqual(
      `\
<p><img src="/assets/images/san-juan-mountains.jpg" title="San Juan Mountains" alt="The San Juan Mountains are beautiful!" /></p>\
`
    );
  });
  it('converts to html an image nested in a link', () => {
    expect(
      htmlConverter.convert(
        parseMarkdown(
          '[![An old rock in the desert](/assets/images/shiprock.jpg "Shiprock, New Mexico by Beau Rogers")](https://www.flickr.com/photos/beaurogers/31833779864/in/photolist-Qv3rFw-34mt9F-a9Cmfy-5Ha3Zi-9msKdv-o3hgjr-hWpUte-4WMsJ1-KUQ8N-deshUb-vssBD-6CQci6-8AFCiD-zsJWT-nNfsgB-dPDwZJ-bn9JGn-5HtSXY-6CUhAL-a4UTXB-ugPum-KUPSo-fBLNm-6CUmpy-4WMsc9-8a7D3T-83KJev-6CQ2bK-nNusHJ-a78rQH-nw3NvT-7aq2qf-8wwBso-3nNceh-ugSKP-4mh4kh-bbeeqH-a7biME-q3PtTf-brFpgb-cg38zw-bXMZc-nJPELD-f58Lmo-bXMYG-bz8AAi-bxNtNT-bXMYi-bXMY6-bXMYv)'
        )
      )
    ).toEqual(
      `\
<p><a href="https://www.flickr.com/photos/beaurogers/31833779864/in/photolist-Qv3rFw-34mt9F-a9Cmfy-5Ha3Zi-9msKdv-o3hgjr-hWpUte-4WMsJ1-KUQ8N-deshUb-vssBD-6CQci6-8AFCiD-zsJWT-nNfsgB-dPDwZJ-bn9JGn-5HtSXY-6CUhAL-a4UTXB-ugPum-KUPSo-fBLNm-6CUmpy-4WMsc9-8a7D3T-83KJev-6CQ2bK-nNusHJ-a78rQH-nw3NvT-7aq2qf-8wwBso-3nNceh-ugSKP-4mh4kh-bbeeqH-a7biME-q3PtTf-brFpgb-cg38zw-bXMZc-nJPELD-f58Lmo-bXMYG-bz8AAi-bxNtNT-bXMYi-bXMY6-bXMYv">\
<img src="/assets/images/shiprock.jpg" title="Shiprock, New Mexico by Beau Rogers" alt="An old rock in the desert" /></a>\
</p>\
`
    );
  });
});
