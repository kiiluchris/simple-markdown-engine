# Simple Markdown Engine

A JavaScript library written in Typescript for conversion of
markdown to other document formats.
This library following the Basic Syntax specified for Markdown by
[Markdown Guide](https://www.markdownguide.org/basic-syntax/).
Currently conversion to HTML is implemented.


The following elements have been implemented in this library:

- Headings
- Paragraphs
- Emphasis: Bold, Italic, and Bold and Italic
- Block quotes: Multi-paragraph and nested block quotes
- Ordered and Unordered Lists
- Code Phrases and Code Blocks
- Horizontal Rules
- Links
- Images

Conversion of images from markdown to LaTeX only works for
local image paths at the moment with the expectation of
images being in the same folder as the generated document
at compile time.
This is due to security-driven limitations in LaTeX
which do not allow the use of remote images.

## Requirements

- Node.js

## Installation

To use with node

```bash
npm install git+https://gitlab.com/kiiluchris/simple-markdown-engine.git
```

Then in the console or a CommonJS file

```js
const markdownEngine = require('simple-markdown-engine')
```

or in an es6 or typescript file

```js
import * as markdownEngine from 'simple-markdown-engine'
```

## Usage

To convert markdown to HTML

```js
import { HtmlConverter } from 'simple-markdown-engine'
const htmlConverter = new HtmlConverter()
const text = "## A Header"
htmlConverter.parseAndConvert(text)
```

To parse markdown to LaTeX
```js
import { LatexConverter } from 'simple-markdown-engine'
const latexConverter = new LatexConverter()
const text = "## A Header"
latexConverter.parseAndConvert(text)
```
