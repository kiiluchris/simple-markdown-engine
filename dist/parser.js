import * as _ from 'ramda';
const SPACE = ' ';
const POUND_SIGN = '#';
const NEW_LINE = '\n';
const EQUAL_SIGN = '=';
const HYPHEN = '-';
const STAR = '*';
const UNDERSCORE = '_';
const LEFT_ARROW = '<';
const RIGHT_ARROW = '>';
const TILDE = '~';
const BACK_TICK = '`';
const BANG = '!';
const LEFT_SQUARE_BRACKET = '[';
const BACKSLASH = '\\';
const makeCharacterMatcher = (expected) => (actual) => {
    return expected === actual;
};
const anyCharacterMatcher = (...matchers) => (actual) => {
    return matchers.some((matcher) => matcher(actual));
};
const characterIsSpace = makeCharacterMatcher(SPACE);
const characterIsStar = makeCharacterMatcher(STAR);
const characterIsUnderscore = makeCharacterMatcher(UNDERSCORE);
const characterIsTilde = makeCharacterMatcher(TILDE);
const characterIsBackTick = makeCharacterMatcher(BACK_TICK);
const characterIsPoundSign = makeCharacterMatcher(POUND_SIGN);
const characterIsBang = makeCharacterMatcher(BANG);
const characterIsLeftBracket = makeCharacterMatcher(LEFT_SQUARE_BRACKET);
const characterIsLeftArrow = makeCharacterMatcher(LEFT_ARROW);
function parseText(text, depth) {
    return {
        tag: 'text',
        depth,
        text,
    };
}
const delimitersAllowingNestedParsing = new Set([UNDERSCORE, TILDE, STAR]);
function parseWrappedText(tag, delimiters) {
    return (line, depth, lineNumber, columnNumber) => {
        var _a;
        const delimiter = delimiters.find((delimiter) => line.startsWith(delimiter));
        let errors = [];
        // No matches found
        if (!delimiter) {
            return [line, null, errors];
        }
        const delimLength = delimiter.length;
        // Look for closing delimiter
        for (let idx = delimLength; idx < line.length; idx++) {
            const chunk = line.slice(idx, idx + delimLength);
            if (chunk === delimiter) {
                const text = line.slice(delimLength, idx);
                const node = {
                    tag,
                    depth,
                    text,
                };
                // When nested parsing is enabled add parsed elements
                // as children
                if (delimitersAllowingNestedParsing.has(delimiter[0])) {
                    const [childNode, childNodeError] = parseLineText(text, depth, lineNumber, columnNumber);
                    errors = childNodeError;
                    const children = childNode.children;
                    if (((_a = children === null || children === void 0 ? void 0 : children[0]) === null || _a === void 0 ? void 0 : _a.text) !== text) {
                        node.children = children;
                    }
                }
                return [line.slice(idx + delimLength), node, errors];
            }
        }
        return [line, null, errors];
    };
}
const linkOrImageRegex = /^(!?)\[(.+)\]\(([^)"]+)(?:\s"([^"]+)")?\)/;
const imageRegex = /^!\[(.+)\]\(([^)"]+)(?:\s"([^"]+)")?\)/;
function parseLinkOrImage(line, depth) {
    if (line[0] !== LEFT_SQUARE_BRACKET &&
        line.slice(0, 2) !== BANG + LEFT_SQUARE_BRACKET) {
        return [line, null, []];
    }
    const linkMatch = linkOrImageRegex.exec(line);
    if (linkMatch === null)
        return [line, null, []];
    const [matchedText, bang, text, href, title] = linkMatch;
    const nestedImageMatch = imageRegex.exec(text);
    const remainingText = line.slice(matchedText.length);
    const tag = bang === BANG ? 'image' : 'link';
    const node = {
        tag,
        depth,
        text,
        metadata: {
            href,
            title,
        },
    };
    if (nestedImageMatch !== null) {
        const [, textNested, hrefNested, titleNested] = nestedImageMatch;
        node.children = [
            {
                tag: 'image',
                depth: depth + 1,
                text: textNested,
                metadata: {
                    href: hrefNested,
                    title: titleNested,
                },
            },
        ];
    }
    return [remainingText, node, []];
}
const urlOrEmailRegex = /^<(?:([\w\d]+@[\w\d]+\.\w+)|(\w+:\/\/[^>]+))>/;
function parseUrlOrEmail(line, depth) {
    const match = urlOrEmailRegex.exec(line);
    if (match === null)
        return [line, null, []];
    const [matchedText, emailGroup, urlGroup] = match;
    const remainingText = line.slice(matchedText.length);
    const text = emailGroup || urlGroup;
    const node = {
        tag: 'link',
        depth,
        text,
        metadata: {
            href: emailGroup ? 'mailto:' + text : text,
        },
    };
    return [remainingText, node, []];
}
const parseItalicPhrase = parseWrappedText('italic', [STAR, UNDERSCORE]);
const parseBoldPhrase = parseWrappedText('bold', [
    STAR.repeat(2),
    UNDERSCORE.repeat(2),
]);
const parseBoldItalicPhrase = parseWrappedText('bold-italic', [
    STAR.repeat(3),
    UNDERSCORE.repeat(3),
]);
const parseStrikethoughPhrase = parseWrappedText('strikethrough', [
    TILDE.repeat(2),
]);
const parseCodeBlockPhrase = parseWrappedText('code', [
    BACK_TICK.repeat(2),
    BACK_TICK,
]);
const wrappedTextParsers = [
    parseBoldItalicPhrase,
    parseBoldPhrase,
    parseItalicPhrase,
    parseCodeBlockPhrase,
    parseStrikethoughPhrase,
    parseLinkOrImage,
    parseUrlOrEmail,
];
const lineTextDelimMatcher = anyCharacterMatcher(characterIsStar, characterIsUnderscore, characterIsBackTick, characterIsLeftBracket, characterIsLeftArrow, characterIsBang, characterIsTilde);
function parseErrorMessage(line) {
    switch (line[0]) {
        case LEFT_SQUARE_BRACKET:
            return 'Incomplete url';
        case BANG:
            return 'Incomplete image';
        case STAR:
        case UNDERSCORE:
            if (line[1] !== line[0]) {
                return 'Incomplete italics';
            }
            else if (line[2] !== line[1]) {
                return 'Incomplete bold';
            }
            else {
                return 'Incomplete bolded italics';
            }
        case LEFT_ARROW:
            return 'Incomplete block quote';
        case TILDE:
            return 'Incomplete strikethrough';
        default:
            return 'Unknown';
    }
}
function parseLineText(line, depth, lineNumber, columnNumber, onlyWrap = false) {
    if (onlyWrap) {
        return [
            {
                tag: 'line',
                depth,
                children: [parseText(line, depth + 1)],
            },
            [],
        ];
    }
    let currentCol = columnNumber;
    const children = [];
    let remainingStringLength = line.length;
    const chars = line.split('');
    const childDepth = depth + 1;
    let previousStringLength = 0;
    const errors = [];
    while (remainingStringLength > 0) {
        const currentIndex = line.length - remainingStringLength;
        const [text, remainingChars] = _.splitWhen(lineTextDelimMatcher, chars.slice(currentIndex));
        currentCol += text.length;
        // If no words left end loop
        if (!text.length && !remainingChars.length)
            break;
        remainingStringLength -= text.length;
        const hasSpaceBefore = text[text.length - 1] === SPACE;
        const hasBaskslashBefore = text[text.length - 1] === BACKSLASH;
        text.length && children.push(parseText(text.join(''), childDepth));
        // Escape next character if a backslash appears before
        // and character is a known delimiter
        if (remainingChars.length && hasBaskslashBefore) {
            currentCol += 1;
            children.push(parseText(remainingChars[0], childDepth));
            remainingChars.shift();
        }
        // Consume all underscores if first one appears in the middle
        // of a word
        if (remainingChars[0] === UNDERSCORE && !hasSpaceBefore) {
            let i = 0;
            for (; i < remainingChars.length; i++) {
                if (remainingChars[i] !== UNDERSCORE) {
                    break;
                }
                children.push(parseText(UNDERSCORE, childDepth));
            }
            currentCol += i + 1;
            remainingChars.splice(0, i);
        }
        // Consume ! character if not followed by a [ character
        if (remainingChars[0] === BANG &&
            remainingChars[1] !== LEFT_SQUARE_BRACKET) {
            children.push(parseText(BANG, childDepth));
            remainingChars.shift();
            currentCol += 1;
        }
        let remainingText = remainingChars.join('');
        for (const fn of wrappedTextParsers) {
            const [textAfterParsing, parsedNode, parseErrors] = fn(remainingText, childDepth, lineNumber, currentCol);
            if (parsedNode) {
                parsedNode && children.push(parsedNode);
                currentCol += remainingText.length - textAfterParsing.length;
                remainingText = textAfterParsing;
            }
            if (parseErrors) {
                parseErrors.push(...parseErrors);
            }
        }
        remainingStringLength = remainingText.length;
        // Escape hatch in case of infinite loop to
        // parse remaining string as text
        if (remainingStringLength &&
            previousStringLength === remainingStringLength) {
            errors.push({
                lineNumber,
                columnNumber,
                message: parseErrorMessage(remainingText),
            });
            children.push(parseText(remainingText, childDepth));
            break;
        }
        previousStringLength = remainingStringLength;
    }
    return [
        {
            tag: 'line',
            depth,
            children,
        },
        errors,
    ];
}
function parseHeaderText(line, depth, lineNumber, columnNumber) {
    const headerPrefix = _.takeWhile(characterIsPoundSign, line.split(''));
    const headerLevel = headerPrefix.length;
    // When invalid prefix process as a line
    if (headerLevel < 1 || headerLevel > 6 || line[headerLevel] !== SPACE) {
        return parseLineText(line, depth, lineNumber, columnNumber);
    }
    const headerText = line.slice(headerLevel + 1);
    const headerLine = [];
    let errors = [];
    if (headerText) {
        const [node, nodeErrors] = parseLineText(headerText, depth + 1, lineNumber, columnNumber);
        headerLine.push(node);
        errors = nodeErrors;
    }
    return [
        {
            tag: `h${headerLevel}`,
            depth: depth,
            children: headerLine,
        },
        errors,
    ];
}
function parseHeaderAltText(line, depth, lineNumber, columnNumber) {
    const chars = line.split('');
    const charCount = chars.reduce((acc, char) => {
        acc[char] = (acc[char] || 0) + 1;
        return acc;
    }, {});
    const [lineNode, lineNodeError] = parseLineText(line, depth, lineNumber, columnNumber);
    if (charCount[EQUAL_SIGN] === chars.length) {
        return [
            {
                tag: 'h1-alt',
                depth,
                children: [lineNode],
            },
            lineNodeError,
        ];
    }
    else if (charCount[HYPHEN] === chars.length) {
        return [
            {
                tag: 'h2-alt',
                depth,
                children: [lineNode],
            },
            lineNodeError,
        ];
    }
    else {
        return [lineNode, lineNodeError];
    }
}
function parseBlockQuoteText(line, depth, lineNumber, columnNumber) {
    var _a;
    const match = (_a = /^([\s>]*>)(.*)/.exec(line)) !== null && _a !== void 0 ? _a : [line, '', line];
    const remainingText = match[2];
    const blockQuotePrefix = match[1].replace(/\s+/g, '');
    const blockQuoteLevel = blockQuotePrefix.length - 1;
    if (!remainingText.length) {
        if (blockQuotePrefix[blockQuoteLevel] === RIGHT_ARROW) {
            // There is a valid prefix so process with
            // empty text in the block quote
            const [childNode, childNodeError] = parseLineText('', blockQuoteLevel + 1, lineNumber, columnNumber);
            return [
                {
                    tag: 'blockquote',
                    depth: blockQuoteLevel,
                    children: [childNode],
                },
                childNodeError,
            ];
        }
        return parseLineText(blockQuotePrefix, depth, lineNumber, columnNumber);
    }
    const [childNode, childNodeError] = parseMarkdownLine(remainingText.slice(1), blockQuoteLevel + 1, lineNumber, columnNumber);
    const rootNode = {
        tag: 'blockquote',
        depth: depth,
        children: [],
    };
    let currentNode = rootNode;
    // Create nested block quote
    for (let currentDepth = depth + 1; currentDepth <= blockQuoteLevel; currentDepth++) {
        const nextNode = {
            tag: 'blockquote',
            depth: currentDepth,
            children: [],
        };
        currentNode.children = [nextNode];
        currentNode = nextNode;
    }
    currentNode.children = [childNode];
    return [rootNode, childNodeError];
}
function parseListItemText(line, depth, tag, lineNumber, columnNumber) {
    const chars = line.split('');
    const [spacePrefix, remaining] = _.splitWhen(_.compose(_.not, characterIsSpace), chars);
    const [, listText] = _.splitWhen(characterIsSpace, remaining);
    // List indent fixed to be after every four spaces
    const listItemLevel = Math.floor(spacePrefix.length / 4);
    if (!listText.length) {
        return parseLineText(line, depth, lineNumber, columnNumber);
    }
    const [childNode, childNodeError] = parseMarkdownLine(listText.slice(1).join(''), depth + 1, lineNumber, columnNumber);
    return [
        {
            tag,
            depth: listItemLevel,
            children: [childNode],
        },
        childNodeError,
    ];
}
function parseCodeBlockText(line, depth, lineNumber, columnNumber) {
    const [childNode, childNodeError] = parseLineText(line, depth + 1, lineNumber, columnNumber, true);
    return [
        {
            tag: 'code',
            depth,
            children: [childNode],
        },
        childNodeError,
    ];
}
function parseHorizontalRule(_line, depth) {
    return [
        {
            tag: 'hr',
            depth,
        },
        [],
    ];
}
function increaseDepth(values, depthOffset = 0) {
    return values.map((value) => {
        value.depth += 1 + depthOffset;
        return Object.assign(Object.assign({}, value), { children: value.children && increaseDepth(value.children, depthOffset) });
    });
}
function fixDepth(node) {
    var _a;
    return Object.assign(Object.assign({}, node), { children: (_a = node.children) === null || _a === void 0 ? void 0 : _a.map((child) => {
            const newChild = Object.assign(Object.assign({}, child), { depth: node.depth + 1 });
            return fixDepth(newChild);
        }) });
}
function createBlockQuotes(blocks) {
    var _a, _b;
    if (!blocks.length) {
        throw new Error('Cannot create BlockQuote from empty array');
    }
    const block = {
        tag: 'blockquote',
        depth: blocks[0].depth,
        children: [],
    };
    // Nest block quotes in tree
    for (let idx = 0; idx < blocks.length; idx++) {
        const current = blocks[idx];
        let parentBlock = block;
        // Create parent nodes if they do not exist
        for (let depth = 1; depth <= current.depth; depth++) {
            if (!((_a = parentBlock.children) === null || _a === void 0 ? void 0 : _a.length) ||
                parentBlock.children[parentBlock.children.length - 1].tag !==
                    'blockquote') {
                (_b = parentBlock.children) === null || _b === void 0 ? void 0 : _b.push({
                    tag: 'blockquote',
                    depth: depth,
                    children: [],
                });
            }
            parentBlock = parentBlock.children[parentBlock.children.length - 1];
        }
        parentBlock.children.push(...current.children);
    }
    return fixDepth(block);
}
function createList(tag, depth) {
    return {
        tag,
        depth,
        children: [],
    };
}
function createLists(items) {
    if (!items.length) {
        throw new Error('Cannot create lists from empty array');
    }
    const createdLists = [];
    const listTypes = new Set(['ul', 'ol']);
    // Group list items
    for (let idx = 0; idx < items.length; idx++) {
        const current = items[idx];
        let parentList = createdLists;
        let parent;
        for (let depth = 0; depth <= current.depth; depth++) {
            // Create a list if none exists
            if (!parentList.length) {
                parentList.push(createList(current.tag, depth));
            }
            else if (parentList[parentList.length - 1].tag === 'li') {
                // When the parent element is an li create a container for either
                // for the ul or ol list items
                parent = parentList[parentList.length - 1];
                parentList = parent.children;
                if (!listTypes.has(parentList[parentList.length - 1].tag)) {
                    parentList.push(createList(current.tag, depth));
                }
            }
            parent = parentList[parentList.length - 1];
            parentList = parent.children;
        }
        parentList.push({
            tag: 'li',
            depth: current.depth,
            children: current.children,
        });
    }
    return createdLists.map(fixDepth);
}
function createParagraph(lines, depthOffset) {
    return {
        tag: 'paragraph',
        depth: 0,
        children: increaseDepth(lines, depthOffset),
    };
}
function createCodeBlocks(blocks) {
    if (!blocks.length)
        throw new Error('Cannot create code block from empty array');
    const block = blocks[0];
    block.children.push(...blocks.slice(1).flatMap((b) => b.children));
    return block;
}
function groupParsedMarkdown(parsedLines) {
    var _a, _b;
    const tree = [];
    for (let i = 0; i < parsedLines.length; i++) {
        const current = parsedLines[i];
        const previousIndex = i - 1;
        switch (current.tag) {
            case 'ol':
            case 'ul': {
                const allowedTags = new Set(['ol', 'ul']);
                const items = _.takeWhile((item) => {
                    return allowedTags.has(item.tag);
                }, parsedLines.slice(i));
                tree.push(...createLists(items));
                i += items.length - 1;
                break;
            }
            case 'code': {
                const codeBlocks = _.takeWhile((line) => {
                    return line.tag === 'code';
                }, parsedLines.slice(i));
                i += codeBlocks.length - 1;
                const block = createCodeBlocks(codeBlocks);
                tree.push(block);
                break;
            }
            case 'blockquote': {
                const blocks = _.takeWhile((block) => {
                    return block.tag === 'blockquote';
                }, parsedLines.slice(i));
                tree.push(createBlockQuotes(blocks));
                i += blocks.length - 1;
                break;
            }
            case 'line': {
                const lines = _.takeWhile((line) => {
                    return line.tag === 'line';
                }, parsedLines.slice(i));
                i += lines.length - 1;
                // Group paragraphs
                const groupedLines = _.groupWith((a, b) => {
                    var _a, _b;
                    return ((_a = a.children) === null || _a === void 0 ? void 0 : _a.length) !== 0 && ((_b = b.children) === null || _b === void 0 ? void 0 : _b.length) !== 0;
                }, lines);
                tree.push(...groupedLines.flatMap((paragraph) => {
                    var _a;
                    if (paragraph.length === 1 && !((_a = paragraph[0].children) === null || _a === void 0 ? void 0 : _a.length)) {
                        return [createParagraph([])];
                    }
                    return [createParagraph(paragraph)];
                }));
                break;
            }
            case 'h1-alt':
            case 'h2-alt': {
                // If header has no text on the previous line
                // assume it is a text node for h1-alt or
                // a horizontal rule for h2-alt
                if (tree.length &&
                    tree[previousIndex].tag === 'paragraph' &&
                    ((_a = tree[previousIndex].children) === null || _a === void 0 ? void 0 : _a.length)) {
                    const previousNode = tree[previousIndex];
                    const previousLine = previousNode.children.pop();
                    if (!((_b = previousNode.children) === null || _b === void 0 ? void 0 : _b.length)) {
                        tree.pop();
                    }
                    const header = {
                        tag: current.tag === 'h1-alt' ? 'h1' : 'h2',
                        depth: current.depth,
                        children: [previousLine],
                    };
                    fixDepth(header);
                    tree.push(header);
                }
                else if (current.tag === 'h2-alt') {
                    tree.push(parseHorizontalRule('', current.depth)[0]);
                }
                else {
                    tree.push(createParagraph(current.children, -1));
                }
                break;
            }
            default: {
                tree.push(current);
                break;
            }
        }
    }
    return tree;
}
const orderedListItemCheckRegex = /^\s*\d+\. /;
const unorderedListItemCheckRegex = /^\s*(-|\*|\+) /;
const codeBlockCheckRegex = /^(\s{4})(\s*[\w\W].+)/;
const horizontalRuleCheck = /^\*\*\*+|___+$/;
function parseMarkdownLine(line, depth, lineNumber, columnNumber) {
    switch (line[0]) {
        case POUND_SIGN:
            return parseHeaderText(line, depth, lineNumber, columnNumber);
        case EQUAL_SIGN:
            return parseHeaderAltText(line, depth, lineNumber, columnNumber);
        case RIGHT_ARROW:
            return parseBlockQuoteText(line, depth, lineNumber, columnNumber);
        default: {
            if (horizontalRuleCheck.test(line)) {
                return parseHorizontalRule(line, depth);
            }
            if (line.slice(0, 2) === HYPHEN.repeat(2)) {
                return parseHeaderAltText(line, depth, lineNumber, columnNumber);
            }
            if (orderedListItemCheckRegex.test(line)) {
                return parseListItemText(line, depth, 'ol', lineNumber, columnNumber);
            }
            if (unorderedListItemCheckRegex.test(line)) {
                return parseListItemText(line, depth, 'ul', lineNumber, columnNumber);
            }
            if (codeBlockCheckRegex.test(line)) {
                return parseCodeBlockText(line.match(codeBlockCheckRegex)[2], depth, lineNumber, columnNumber);
            }
            return parseLineText(line, depth, lineNumber, columnNumber);
        }
    }
}
export function parseMarkdown(text, depth = 0) {
    return parseMarkdownStrict(text, depth)[0];
}
export function parseMarkdownStrict(text, depth = 0) {
    const parsedLinesWithErrors = text.split(NEW_LINE).map((line, idx) => {
        return parseMarkdownLine(line, depth, idx + 1, 1);
    });
    const [parsedLines, errors] = _.transpose(parsedLinesWithErrors);
    return [groupParsedMarkdown(parsedLines), errors.flat()];
}
//# sourceMappingURL=parser.js.map