import { MarkdownTree } from './types';
declare type ParseError = {
    lineNumber: number;
    columnNumber: number;
    message: string;
};
export declare function parseMarkdown(text: string, depth?: number): MarkdownTree[];
export declare function parseMarkdownStrict(text: string, depth?: number): [MarkdownTree[], ParseError[]];
export {};
//# sourceMappingURL=parser.d.ts.map