import { MarkdownConverter } from './base';
import { wrapWithTags, wrapWithTagsWithNewline } from './util';
const header = `
\\documentclass{article}
\\usepackage{babel}
\\usepackage{hyperref}
\\usepackage{graphicx}
\\usepackage{pdfcomment}
\\usepackage{listings}
\\usepackage[normalem]{ulem}

\\graphicspath{ {./} }

\\setlength\\parindent{0pt}

\\begin{document}

`;
const footer = `
\\end{document}
`;
export class LaTeXConverter extends MarkdownConverter {
    constructor() {
        super(...arguments);
        this.header = header;
        this.footer = footer;
    }
    tags(tag) {
        switch (tag) {
            case 'italic':
                return ['\\textit{', '}'];
            case 'bold':
                return ['\\textbf{', '}'];
            case 'strikethrough':
                return ['\\sout{', '}'];
            case 'bold-italic':
                return ['\\textbf{\\textit{', '}}'];
            case 'h1':
                return ['\\section{', '}\n'];
            case 'h2':
                return ['\\subsection{', '}\n'];
            case 'h3':
            case 'h4':
            case 'h5':
            case 'h6':
                return ['\\subsubsection{', '}\n'];
            case 'blockquote':
                return ['\\begin{quote}\n', '\\end{quote}\n'];
            case 'li':
                return ['\\item ', ''];
            case 'ol':
                return ['\\begin{enumerate}\n', '\\end{enumerate}\n'];
            case 'ul':
                return ['\\begin{itemize}\n', '\\end{itemize}\n'];
            case 'code':
                return ['\\lstinline{', '}'];
            case 'paragraph':
                return ['', '\n\n'];
            case 'hr':
            case 'image':
            case 'link':
            default:
                return ['', ''];
        }
    }
    convertNode(node, level) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
        const tags = this.tags(node.tag);
        const indent = ' '.repeat(level * 4);
        const tagsWithIndent = [indent + tags[0], indent + tags[1]];
        switch (node.tag) {
            case 'hr':
                return '\\par\\noindent\\rule{\\textwidth}{0.4pt}';
            case 'link': {
                const title = ((_a = node.metadata) === null || _a === void 0 ? void 0 : _a.href) || '';
                const href = ((_b = node.metadata) === null || _b === void 0 ? void 0 : _b.href) || '';
                const content = node.children ? this.convert(node.children) : node.text;
                return `\\pdftooltip{\\href{${href}}{${content}}}{${title}}`;
            }
            case 'image': {
                const title = ((_c = node.metadata) === null || _c === void 0 ? void 0 : _c.href) || '';
                const href = ((_d = node.metadata) === null || _d === void 0 ? void 0 : _d.href) || '';
                const content = node.text;
                return `\\pdftooltip{\\includegraphics{${href}}}{${title || content}}`;
            }
            case 'italic':
            case 'bold':
            case 'bold-italic':
            case 'strikethrough': {
                const text = ((_e = node.children) === null || _e === void 0 ? void 0 : _e.length)
                    ? node.children
                        .map((child) => {
                        return this.convertNode(child, level + 1);
                    })
                        .join('')
                    : node.text;
                return wrapWithTags(text || '', ...tags);
            }
            case 'code': {
                if (node.children) {
                    const text = node.children
                        .map((child) => {
                        return indent + this.convertNode(child, level + 1);
                    })
                        .join('\n');
                    if (level === 0) {
                        return wrapWithTagsWithNewline(text, '\\begin{lstlisting}\n', '\n\\end{lstlisting}');
                    }
                    return indent + text;
                }
                else {
                    return wrapWithTags(node.text || '', ...tags);
                }
            }
            case 'text':
                return node.text || '';
            case 'line':
                return (indent +
                    ((_f = node.children) === null || _f === void 0 ? void 0 : _f.map((child) => {
                        return this.convertNode(child, level + 1);
                    }).join('')) || '');
            case 'paragraph': {
                if (!((_g = node.children) === null || _g === void 0 ? void 0 : _g.length)) {
                    return '';
                }
                const lines = node.children
                    .map((line) => {
                    return this.convertNode(line, level + 1).trimStart();
                })
                    .join('\n\n');
                return wrapWithTags(lines, ...tags);
            }
            case 'h1':
            case 'h2':
            case 'h3':
            case 'h4':
            case 'h5':
            case 'h6': {
                const text = ((_h = node.children) === null || _h === void 0 ? void 0 : _h.map((child) => {
                    return this.convertNode(child, level + 1).trimStart();
                }).join('')) || '';
                return wrapWithTags(text, ...tags);
            }
            case 'li': {
                const text = ((_j = node.children) === null || _j === void 0 ? void 0 : _j.map((child) => {
                    return this.convertNode(child, level + 1);
                }).join('\n')) || '';
                return indent + wrapWithTags(text, ...tags);
            }
            case 'blockquote': {
                const text = (_k = node.children) === null || _k === void 0 ? void 0 : _k.flatMap((child) => {
                    const text = this.convertNode(child, level + 1);
                    if (!text) {
                        return [];
                    }
                    return [text];
                }).join('\n');
                return wrapWithTagsWithNewline(text || '', (level === 0 ? '' : '\n') + tagsWithIndent[0], '\n' + tagsWithIndent[1]);
            }
            case 'ol':
            case 'ul': {
                const text = (_l = node.children) === null || _l === void 0 ? void 0 : _l.flatMap((child) => {
                    const text = this.convertNode(child, level + 1);
                    if (!text) {
                        return [];
                    }
                    return [text];
                }).join('\n');
                return wrapWithTagsWithNewline(text || '', (level === 0 ? '' : '\n') + tagsWithIndent[0], '\n' + tagsWithIndent[1]);
            }
        }
        return '';
    }
}
//# sourceMappingURL=latex.js.map