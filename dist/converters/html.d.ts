import type { MarkdownElementTag, MarkdownTree } from '../types';
import { MarkdownConverter } from './base';
export declare class HtmlConverter extends MarkdownConverter {
    tags(tag: MarkdownElementTag): [string, string];
    convertNode(node: MarkdownTree): string;
}
//# sourceMappingURL=html.d.ts.map