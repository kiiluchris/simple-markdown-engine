import { parseMarkdownStrict } from '../parser';
export class MarkdownConverter {
    constructor() {
        this.header = '';
        this.footer = '';
    }
    convert(nodes) {
        const body = nodes
            .flatMap((node) => {
            const text = this.convertNode(node, 0);
            if (!text) {
                return [];
            }
            return [text];
        })
            .join('\n');
        return this.header + body + this.footer;
    }
    parseAndConvert(text) {
        return this.parseAndConvertStrict(text)[0];
    }
    parseAndConvertStrict(text) {
        const [nodes, errors] = parseMarkdownStrict(text);
        return [this.convert(nodes), errors];
    }
}
//# sourceMappingURL=base.js.map