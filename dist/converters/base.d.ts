import { MarkdownElementTag, MarkdownTree, ParseError } from '../types';
export declare abstract class MarkdownConverter {
    header: string;
    footer: string;
    abstract tags(tag: MarkdownElementTag): [string, string];
    abstract convertNode(node: MarkdownTree, level: number): string;
    convert(nodes: MarkdownTree[]): string;
    parseAndConvert(text: string): string;
    parseAndConvertStrict(text: string): [string, ParseError[]];
}
//# sourceMappingURL=base.d.ts.map