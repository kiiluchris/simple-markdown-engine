export declare function wrapWithTags(text: string, openingTag: string, closingTag: string): string;
export declare function wrapWithTagsWithNewline(text: string, openingTag: string, closingTag: string): string;
//# sourceMappingURL=util.d.ts.map