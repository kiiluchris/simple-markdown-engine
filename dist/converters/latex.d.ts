import type { MarkdownElementTag, MarkdownTree } from '../types';
import { MarkdownConverter } from './base';
export declare class LaTeXConverter extends MarkdownConverter {
    header: string;
    footer: string;
    tags(tag: MarkdownElementTag): [string, string];
    convertNode(node: MarkdownTree, level: number): string;
}
//# sourceMappingURL=latex.d.ts.map