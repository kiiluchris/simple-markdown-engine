export function wrapWithTags(text, openingTag, closingTag) {
    return `${openingTag}${text}${closingTag}`;
}
export function wrapWithTagsWithNewline(text, openingTag, closingTag) {
    return wrapWithTags(text, openingTag + '\n', '\n' + closingTag);
}
//# sourceMappingURL=util.js.map