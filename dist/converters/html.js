import { MarkdownConverter } from './base';
import { wrapWithTags, wrapWithTagsWithNewline } from './util';
export class HtmlConverter extends MarkdownConverter {
    tags(tag) {
        switch (tag) {
            case 'strikethrough':
                return ['<s>', '</s>'];
            case 'italic':
                return ['<em>', '</em>'];
            case 'bold':
                return ['<strong>', '</strong>'];
            case 'bold-italic':
                return ['<strong><em>', '</em></strong>'];
            case 'paragraph':
                return ['<p>', '</p>'];
            case 'h1':
            case 'h2':
            case 'h3':
            case 'h4':
            case 'h5':
            case 'h6':
            case 'blockquote':
            case 'li':
            case 'ol':
            case 'ul':
            case 'code':
                return [`<${tag}>`, `</${tag}>`];
            case 'hr':
                return [`<${tag}/>`, `<${tag}/>`];
            case 'image':
                return ['<img/>', '<img/>'];
            case 'link':
                return ['<a/>', '<a/>'];
            default:
                return ['', ''];
        }
    }
    convertNode(node) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
        const tags = this.tags(node.tag);
        switch (node.tag) {
            case 'hr':
                return '<hr/>';
            case 'link': {
                let title = ((_a = node.metadata) === null || _a === void 0 ? void 0 : _a.title) || '';
                let href = ((_b = node.metadata) === null || _b === void 0 ? void 0 : _b.href) || '';
                if (href) {
                    href = ` href="${href}"`;
                }
                if (title) {
                    title = ` title="${title}"`;
                }
                return `<a${href}${title}>${node.children ? this.convert(node.children) : node.text}</a>`;
            }
            case 'image': {
                let title = ((_c = node.metadata) === null || _c === void 0 ? void 0 : _c.title) || '';
                let href = ((_d = node.metadata) === null || _d === void 0 ? void 0 : _d.href) || '';
                let text = node.text || '';
                if (href) {
                    href = ` src="${href}"`;
                }
                if (title) {
                    title = ` title="${title}"`;
                }
                if (text) {
                    text = ` alt="${text}"`;
                }
                return `<img${href}${title}${text} />`;
            }
            case 'italic':
            case 'bold':
            case 'bold-italic':
            case 'strikethrough': {
                const text = ((_e = node.children) === null || _e === void 0 ? void 0 : _e.length)
                    ? node.children
                        .map((child) => {
                        return this.convertNode(child);
                    })
                        .join('')
                    : node.text;
                return wrapWithTags(text || '', ...tags);
            }
            case 'code': {
                if (node.children) {
                    const text = node.children
                        .map((child) => {
                        return this.convertNode(child)
                            .replace(/&/g, '&amp;')
                            .replace(/</g, '&lt;')
                            .replace(/>/g, '&gt;')
                            .replace(/"/g, '&quot;')
                            .replace(/'/g, '&#039;');
                    })
                        .join('<br/>');
                    return wrapWithTagsWithNewline(text, ...wrapTags(['pre'], ...tags));
                }
                else {
                    return wrapWithTags(node.text || '', ...tags);
                }
            }
            case 'text':
                return node.text || '';
            case 'line':
                return (((_f = node.children) === null || _f === void 0 ? void 0 : _f.map((child) => {
                    return this.convertNode(child);
                }).join('')) || '');
            case 'paragraph': {
                if (!((_g = node.children) === null || _g === void 0 ? void 0 : _g.length)) {
                    return '';
                }
                const lines = node.children
                    .map((line) => {
                    return this.convertNode(line);
                })
                    .join('<br/>');
                return wrapWithTags(lines, ...tags);
            }
            case 'h1':
            case 'h2':
            case 'h3':
            case 'h4':
            case 'h5':
            case 'h6': {
                const text = ((_h = node.children) === null || _h === void 0 ? void 0 : _h.map((child) => {
                    return this.convertNode(child);
                }).join('')) || '';
                return wrapWithTags(text, ...tags);
            }
            case 'li': {
                const text = ((_j = node.children) === null || _j === void 0 ? void 0 : _j.map((child) => {
                    return this.convertNode(child);
                }).join('\n')) || '';
                return wrapWithTags(text, ...tags);
            }
            case 'blockquote': {
                const text = ((_k = node.children) === null || _k === void 0 ? void 0 : _k.flatMap((child) => {
                    const text = this.convertNode(child);
                    if (!text) {
                        return [];
                    }
                    if (child.tag === 'line') {
                        return [wrapWithTags(text, ...this.tags('paragraph'))];
                    }
                    return [text];
                }).join('\n')) || '';
                return wrapWithTagsWithNewline(text, ...tags);
            }
            case 'ol':
            case 'ul': {
                const text = ((_l = node.children) === null || _l === void 0 ? void 0 : _l.flatMap((child) => {
                    const text = this.convertNode(child);
                    if (!text) {
                        return [];
                    }
                    return [text];
                }).join('\n')) || '';
                return wrapWithTagsWithNewline(text, ...tags);
            }
        }
        return '';
    }
}
function wrapTags(parents, openingTag, closingTag) {
    const newOpeningTag = parents.reduceRight((tag, parent) => {
        return `<${parent}>${tag}`;
    }, openingTag);
    const newClosingTag = parents.reduce((tag, parent) => {
        return `${tag}</${parent}>`;
    }, closingTag);
    return [newOpeningTag, newClosingTag];
}
//# sourceMappingURL=html.js.map