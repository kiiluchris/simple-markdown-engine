export type MarkdownElementTag =
  | 'text'
  | 'line'
  | 'paragraph'
  | 'blockquote'
  | 'h1-alt'
  | 'h2-alt'
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'h6'
  | 'bold'
  | 'italic'
  | 'bold-italic'
  | 'strikethrough'
  | 'ol'
  | 'ul'
  | 'li'
  | 'code'
  | 'hr'
  | 'link'
  | 'image';

type ImageMetadata = {
  href: string;
  title?: string;
};

export type Metadata = ImageMetadata;

export type MarkdownTree = {
  tag: MarkdownElementTag;
  depth: number;
  text?: string;
  children?: MarkdownTree[];
  metadata?: Metadata;
};

export type ParseError = {
  lineNumber: number;
  columnNumber: number;
  message: string;
};
