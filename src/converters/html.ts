import type { MarkdownElementTag, MarkdownTree } from '../types';
import { MarkdownConverter } from './base';
import { wrapWithTags, wrapWithTagsWithNewline } from './util';

export class HtmlConverter extends MarkdownConverter {
  tags(tag: MarkdownElementTag): [string, string] {
    switch (tag) {
      case 'strikethrough':
        return ['<s>', '</s>'];
      case 'italic':
        return ['<em>', '</em>'];
      case 'bold':
        return ['<strong>', '</strong>'];
      case 'bold-italic':
        return ['<strong><em>', '</em></strong>'];
      case 'paragraph':
        return ['<p>', '</p>'];
      case 'h1':
      case 'h2':
      case 'h3':
      case 'h4':
      case 'h5':
      case 'h6':
      case 'blockquote':
      case 'li':
      case 'ol':
      case 'ul':
      case 'code':
        return [`<${tag}>`, `</${tag}>`];
      case 'hr':
        return [`<${tag}/>`, `<${tag}/>`];
      case 'image':
        return ['<img/>', '<img/>'];
      case 'link':
        return ['<a/>', '<a/>'];
      default:
        return ['', ''];
    }
  }

  convertNode(node: MarkdownTree): string {
    const tags = this.tags(node.tag);
    switch (node.tag) {
      case 'hr':
        return '<hr/>';
      case 'link': {
        let title = node.metadata?.title || '';
        let href = node.metadata?.href || '';
        if (href) {
          href = ` href="${href}"`;
        }
        if (title) {
          title = ` title="${title}"`;
        }
        return `<a${href}${title}>${
          node.children ? this.convert(node.children) : node.text
        }</a>`;
      }
      case 'image': {
        let title = node.metadata?.title || '';
        let href = node.metadata?.href || '';
        let text = node.text || '';
        if (href) {
          href = ` src="${href}"`;
        }
        if (title) {
          title = ` title="${title}"`;
        }
        if (text) {
          text = ` alt="${text}"`;
        }
        return `<img${href}${title}${text} />`;
      }
      case 'italic':
      case 'bold':
      case 'bold-italic':
      case 'strikethrough': {
        const text = node.children?.length
          ? node.children
              .map((child) => {
                return this.convertNode(child);
              })
              .join('')
          : node.text;
        return wrapWithTags(text || '', ...tags);
      }
      case 'code': {
        if (node.children) {
          const text = node.children
            .map((child) => {
              return this.convertNode(child)
                .replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#039;');
            })
            .join('<br/>');
          return wrapWithTagsWithNewline(text, ...wrapTags(['pre'], ...tags));
        } else {
          return wrapWithTags(node.text || '', ...tags);
        }
      }
      case 'text':
        return node.text || '';
      case 'line':
        return (
          node.children
            ?.map((child) => {
              return this.convertNode(child);
            })
            .join('') || ''
        );
      case 'paragraph': {
        if (!node.children?.length) {
          return '';
        }
        const lines = node.children
          .map((line) => {
            return this.convertNode(line);
          })
          .join('<br/>');
        return wrapWithTags(lines, ...tags);
      }
      case 'h1':
      case 'h2':
      case 'h3':
      case 'h4':
      case 'h5':
      case 'h6': {
        const text =
          node.children
            ?.map((child) => {
              return this.convertNode(child);
            })
            .join('') || '';
        return wrapWithTags(text, ...tags);
      }
      case 'li': {
        const text =
          node.children
            ?.map((child) => {
              return this.convertNode(child);
            })
            .join('\n') || '';
        return wrapWithTags(text, ...tags);
      }
      case 'blockquote': {
        const text =
          node.children
            ?.flatMap((child) => {
              const text = this.convertNode(child);
              if (!text) {
                return [];
              }
              if (child.tag === 'line') {
                return [wrapWithTags(text, ...this.tags('paragraph'))];
              }
              return [text];
            })
            .join('\n') || '';
        return wrapWithTagsWithNewline(text, ...tags);
      }
      case 'ol':
      case 'ul': {
        const text =
          node.children
            ?.flatMap((child) => {
              const text = this.convertNode(child);
              if (!text) {
                return [];
              }
              return [text];
            })
            .join('\n') || '';
        return wrapWithTagsWithNewline(text, ...tags);
      }
    }
    return '';
  }
}

function wrapTags(
  parents: string[],
  openingTag: string,
  closingTag: string
): [string, string] {
  const newOpeningTag = parents.reduceRight((tag, parent) => {
    return `<${parent}>${tag}`;
  }, openingTag);
  const newClosingTag = parents.reduce((tag, parent) => {
    return `${tag}</${parent}>`;
  }, closingTag);
  return [newOpeningTag, newClosingTag];
}
