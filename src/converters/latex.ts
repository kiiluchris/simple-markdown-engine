import type { MarkdownElementTag, MarkdownTree } from '../types';
import { MarkdownConverter } from './base';
import { wrapWithTags, wrapWithTagsWithNewline } from './util';

const header = `
\\documentclass{article}
\\usepackage{babel}
\\usepackage{hyperref}
\\usepackage{graphicx}
\\usepackage{pdfcomment}
\\usepackage{listings}
\\usepackage[normalem]{ulem}

\\graphicspath{ {./} }

\\setlength\\parindent{0pt}

\\begin{document}

`;
const footer = `
\\end{document}
`;

export class LaTeXConverter extends MarkdownConverter {
  header = header;
  footer = footer;

  tags(tag: MarkdownElementTag): [string, string] {
    switch (tag) {
      case 'italic':
        return ['\\textit{', '}'];
      case 'bold':
        return ['\\textbf{', '}'];
      case 'strikethrough':
        return ['\\sout{', '}'];
      case 'bold-italic':
        return ['\\textbf{\\textit{', '}}'];
      case 'h1':
        return ['\\section{', '}\n'];
      case 'h2':
        return ['\\subsection{', '}\n'];
      case 'h3':
      case 'h4':
      case 'h5':
      case 'h6':
        return ['\\subsubsection{', '}\n'];
      case 'blockquote':
        return ['\\begin{quote}\n', '\\end{quote}\n'];
      case 'li':
        return ['\\item ', ''];
      case 'ol':
        return ['\\begin{enumerate}\n', '\\end{enumerate}\n'];
      case 'ul':
        return ['\\begin{itemize}\n', '\\end{itemize}\n'];
      case 'code':
        return ['\\lstinline{', '}'];
      case 'paragraph':
        return ['', '\n\n'];
      case 'hr':
      case 'image':
      case 'link':
      default:
        return ['', ''];
    }
  }

  convertNode(node: MarkdownTree, level: number): string {
    const tags = this.tags(node.tag);
    const indent = ' '.repeat(level * 4);
    const tagsWithIndent = [indent + tags[0], indent + tags[1]];
    switch (node.tag) {
      case 'hr':
        return '\\par\\noindent\\rule{\\textwidth}{0.4pt}';
      case 'link': {
        const title = node.metadata?.href || '';
        const href = node.metadata?.href || '';
        const content = node.children ? this.convert(node.children) : node.text;
        return `\\pdftooltip{\\href{${href}}{${content}}}{${title}}`;
      }
      case 'image': {
        const title = node.metadata?.href || '';
        const href = node.metadata?.href || '';
        const content = node.text;
        return `\\pdftooltip{\\includegraphics{${href}}}{${title || content}}`;
      }
      case 'italic':
      case 'bold':
      case 'bold-italic':
      case 'strikethrough': {
        const text = node.children?.length
          ? node.children
              .map((child) => {
                return this.convertNode(child, level + 1);
              })
              .join('')
          : node.text;
        return wrapWithTags(text || '', ...tags);
      }
      case 'code': {
        if (node.children) {
          const text = node.children
            .map((child) => {
              return indent + this.convertNode(child, level + 1);
            })
            .join('\n');
          if (level === 0) {
            return wrapWithTagsWithNewline(
              text,
              '\\begin{lstlisting}\n',
              '\n\\end{lstlisting}'
            );
          }
          return indent + text;
        } else {
          return wrapWithTags(node.text || '', ...tags);
        }
      }
      case 'text':
        return node.text || '';
      case 'line':
        return (
          indent +
            node.children
              ?.map((child) => {
                return this.convertNode(child, level + 1);
              })
              .join('') || ''
        );
      case 'paragraph': {
        if (!node.children?.length) {
          return '';
        }
        const lines = node.children
          .map((line) => {
            return this.convertNode(line, level + 1).trimStart();
          })
          .join('\n\n');
        return wrapWithTags(lines, ...tags);
      }
      case 'h1':
      case 'h2':
      case 'h3':
      case 'h4':
      case 'h5':
      case 'h6': {
        const text =
          node.children
            ?.map((child) => {
              return this.convertNode(child, level + 1).trimStart();
            })
            .join('') || '';
        return wrapWithTags(text, ...tags);
      }
      case 'li': {
        const text =
          node.children
            ?.map((child) => {
              return this.convertNode(child, level + 1);
            })
            .join('\n') || '';
        return indent + wrapWithTags(text, ...tags);
      }
      case 'blockquote': {
        const text = node.children
          ?.flatMap((child) => {
            const text = this.convertNode(child, level + 1);
            if (!text) {
              return [];
            }
            return [text];
          })
          .join('\n');
        return wrapWithTagsWithNewline(
          text || '',
          (level === 0 ? '' : '\n') + tagsWithIndent[0],
          '\n' + tagsWithIndent[1]
        );
      }
      case 'ol':
      case 'ul': {
        const text = node.children
          ?.flatMap((child) => {
            const text = this.convertNode(child, level + 1);
            if (!text) {
              return [];
            }
            return [text];
          })
          .join('\n');
        return wrapWithTagsWithNewline(
          text || '',
          (level === 0 ? '' : '\n') + tagsWithIndent[0],
          '\n' + tagsWithIndent[1]
        );
      }
    }
    return '';
  }
}
