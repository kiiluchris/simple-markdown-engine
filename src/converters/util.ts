export function wrapWithTags(
  text: string,
  openingTag: string,
  closingTag: string
) {
  return `${openingTag}${text}${closingTag}`;
}

export function wrapWithTagsWithNewline(
  text: string,
  openingTag: string,
  closingTag: string
) {
  return wrapWithTags(text, openingTag + '\n', '\n' + closingTag);
}
