import { parseMarkdownStrict } from '../parser';
import { MarkdownElementTag, MarkdownTree, ParseError } from '../types';

export abstract class MarkdownConverter {
  header = '';
  footer = '';

  abstract tags(tag: MarkdownElementTag): [string, string];
  abstract convertNode(node: MarkdownTree, level: number): string;

  convert(nodes: MarkdownTree[]): string {
    const body = nodes
      .flatMap((node) => {
        const text = this.convertNode(node, 0);
        if (!text) {
          return [];
        }
        return [text];
      })
      .join('\n');
    return this.header + body + this.footer;
  }

  parseAndConvert(text: string): string {
    return this.parseAndConvertStrict(text)[0];
  }

  parseAndConvertStrict(text: string): [string, ParseError[]] {
    const [nodes, errors] = parseMarkdownStrict(text);
    return [this.convert(nodes), errors];
  }
}
